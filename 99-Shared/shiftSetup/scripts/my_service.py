"""Example for library that queries a site-specific web service
"""
import urllib2, sys, _socket

# Created by Kay Kasemir. Latest commit e83a5bd on 12 Jul 2018. Created for urllib.
# Modified slightly by Benjamin Bolling, 27 September 2021. Replaced urllib by urllib2.

# Hack for jython's urllib, see https://github.com/PythonScanClient/PyScanClient/issues/18

def checkSocketLib():
    # Workaround: Detect closed NIO_GROUP and re-create it
    try:
        if _socket.NIO_GROUP.isShutdown():
            _socket.NIO_GROUP = _socket.NioEventLoopGroup(2, _socket.DaemonThreadFactory("PyScan-Netty-Client-%s"))
            sys.registerCloser(_socket._shutdown_threadpool)
    except AttributeError:
        raise Exception("Jython _socket.py has changed from jython_2.7.0")

def read_html(u):
    checkSocketLib()
    f = urllib2.urlopen(u)
    return f.read()
