from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil
from org.phoebus.framework.macros import MacroHandler

import sys

macros = {}
pathparts = {}

pathparts["RFQ-010:RFS-Cav-110"] = "RFQ"
pathparts["MEBT-010:EMR-Cav-001"] = "MEBT_010"
pathparts["MEBT-010:EMR-Cav-002"] = "MEBT_020"
pathparts["MEBT-010:EMR-Cav-003"] = "MEBT_030"
pathparts["DTL-010:RFS-Cav-110"] = "DTL_010"
pathparts["DTL-020:RFS-Cav-110"] = "DTL_020"
pathparts["DTL-030:RFS-Cav-110"] = "DTL_030"
pathparts["DTL-040:RFS-Cav-110"] = "DTL_040"
pathparts["DTL-050:RFS-Cav-110"] = "DTL_050"

pathpart = pathparts[PVUtil.getString(pvs[0])]

sys.path.insert(1, '/opt/opis/20-SystemExpert/10-ACC/999-RF/99-Shared/overview/scripts/{}'.format('-'.join(pathpart.split('_'))))
sys.path.insert(1, '/opt/opis/20-SystemExpert/10-ACC/999-RF/99-Shared/rflps/scripts')

macros['LLRF'] = __import__("_llrf_macros_{}".format(pathpart)).get()
macros['TIM'] = __import__("_timing_macros_{}".format(pathpart)).get()
macros['LPS'] = __import__("_rflps_macros_{}".format(pathpart)).get()

for btn,macro in macros.items():
    for k,v in macro.items():
        if k=="path": v="/opt/opis/20-SystemExpert/10-ACC/999-RF/99-Shared/{}".format(v.split("..")[-1])
        ScriptUtil.findWidgetByName(widget, btn).getPropertyValue('macros').add(k,v)
