# Must be in same folder as symbols when running.

PVtypes   = {'AptM':'APTM_bw.svg',
            'BCM':'BCM_bw.svg',
            'BLM':'BLM_bw.svg', # or ICBLM_bw.svg - perhaps a second dictionary is needed
            'BPM':'BPM_bw.svg',
            'CavH':'CAVH_bw.svg',
            'CavM':'CAVM_bw.svg',
            'CavS':'CAVS_bw.svg',
            'Cav':'CAV_bw.svg',
            'CH':'COR_bw.svg',
            'CV':'COR_bw.svg',
            'CX':'COR_bw.svg',
            'ChDump':'Dump_bw.svg', # regular dump ?
            'Chop':'CHOP_bw.svg',
            'Coll':'Coll_bw.svg',
            'Collimator':'Coll_bw.svg',
            'Dpl':'BIF_bw.svg',
            'DV':'BND_bw.svg',
            'EMU':'EMU_bw.svg',
            'FC':'FC_bw.svg',
            'IBS':'IBS_bw.svg',
            'ICBLM':'ICBLM_bw.svg',
            'Img':'IMG_bw.svg',
            'Iris':'IRIS_bw.svg',
            'LBM':'LBM_bw.svg',
            'NPM':'BIF_bw.svg',
            'ParOpen':'ParOpen.svg',
            'ParClose':'ParClose.svg',
            'QH':'QH_bw.svg',
            'QH2':'QH2.svg',
            'QV':'QV_bw.svg',
            'QV2':'QV2.svg',
            'RstH':'RTS_bw.svg',
            'RstV':'RTS_bw.svg',
            'Sol':'Sol_bw.svg',
            'Source':'IS_bw.svg',
            'Tgr':'Tgt_bw.svg',
            'VVMC':'VAL_bw.svg',
            'VVS':'VAL_bw.svg',
            'WS':'WS_bw.svg'}

keys = []
icons = []
aspRs = []

for key, value in PVtypes.items():
    vv = value.split(".svg")
    if len(vv) == 2:
        keys.append(key)
        icons.append(value)

for icon in icons:
    with open(icon, 'r') as file:
        data = file.read().replace('\n', '')
    size = list(map(float, str(data.split("><defs><")[0].split("viewBox=")[1].split('"')[1]).split(" ")))
    aspR = float("{:.4f}".format(size[2]/size[3])) # w = size[2] # h = size[3]
    aspRs.append(aspR)

IconWidths = dict(zip(keys, aspRs))
IconWidths[None] = 0

print(IconWidths)
