from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil
from org.phoebus.framework.macros import MacroHandler
import sys

# Section and subsection are fixed per script
macros = {}

for i in range(1,4):
    sys.path.insert(1,'/opt/opis/20-SystemExpert/10-ACC/999-RF/99-Shared/overview/scripts/MEBT-0{}0'.format(i,i))



##################################################################################################
#	Create the list of all buttons
##################################################################################################

buttons_lst = [ 'buncher1','buncher2','buncher3']

##################################################################################################
#	Hardcoded macro data
##################################################################################################

## Macros for buncher 1 rflps
import _rflps_macros_MEBT_010
macros['buncher1'] = _rflps_macros_MEBT_010.get()

## Macros for buncher 2 rflps
import _rflps_macros_MEBT_020
macros['buncher2'] = _rflps_macros_MEBT_020.get()

## Macros for buncher 3 rflps
import _rflps_macros_MEBT_030
macros['buncher3'] = _rflps_macros_MEBT_030.get()

##################################################################################################
# Send the macros to each respective widget, or disable the buttons
##################################################################################################

buttons = {}
groups = {}
for btn in buttons_lst:
    buttons['{}_button'.format(btn)]=ScriptUtil.findWidgetByName(widget, '{}_btn'.format(btn))
    groups['{}_button'.format(btn)]=ScriptUtil.findWidgetByName(widget, btn)

    for k,v in macros[btn].items():
        groups['{}_button'.format(btn)].getPropertyValue('macros').add(k,v)



buttons = {}
groups = {}
for btn in buttons_lst:
    buttons['{}_button'.format(btn)]=ScriptUtil.findWidgetByName(widget, '{}_btn'.format(btn))
    groups['{}_button'.format(btn)]=ScriptUtil.findWidgetByName(widget, btn)
    for k,v in macros[btn].items():
        groups['{}_button'.format(btn)].getPropertyValue('macros').add(k,v)
