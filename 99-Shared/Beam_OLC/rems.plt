<?xml version="1.0" encoding="UTF-8"?>
<databrowser>
  <title></title>
  <show_legend>true</show_legend>
  <show_toolbar>true</show_toolbar>
  <grid>true</grid>
  <update_period>3.0</update_period>
  <scroll_step>5</scroll_step>
  <scroll>true</scroll>
  <start>-1 hours</start>
  <end>now</end>
  <archive_rescale>STAGGER</archive_rescale>
  <foreground>
    <red>26</red>
    <green>26</green>
    <blue>26</blue>
  </foreground>
  <background>
    <red>220</red>
    <green>225</green>
    <blue>221</blue>
  </background>
  <title_font>Source Sans Pro Semibold|24|0</title_font>
  <label_font>Source Sans Pro Semibold|18|0</label_font>
  <scale_font>Source Sans Pro|18|0</scale_font>
  <legend_font>Source Sans Pro|16|0</legend_font>
  <axes>
    <axis>
      <visible>true</visible>
      <name>Integrated Dose</name>
      <use_axis_name>true</use_axis_name>
      <use_trace_names>false</use_trace_names>
      <right>false</right>
      <color>
        <red>0</red>
        <green>0</green>
        <blue>0</blue>
      </color>
      <min>0.0</min>
      <max>10.0</max>
      <grid>false</grid>
      <autoscale>true</autoscale>
      <log_scale>true</log_scale>
    </axis>
  </axes>
  <annotations>
  </annotations>
  <pvlist>
    <pv>
      <display_name>$(name1)</display_name>
      <visible>true</visible>
      <name>$(P1)$(R1)AvgValue</name>
      <axis>0</axis>
      <color>
        <red>0</red>
        <green>0</green>
        <blue>255</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
      <archive>
        <name>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</name>
        <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
        <key>1</key>
      </archive>
    </pv>
    <pv>
      <display_name>$(name2)</display_name>
      <visible>true</visible>
      <name>$(P2)$(R2)AvgValue</name>
      <axis>0</axis>
      <color>
        <red>179</red>
        <green>128</green>
        <blue>26</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
      <archive>
        <name>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</name>
        <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
        <key>1</key>
      </archive>
    </pv>
    <pv>
      <display_name>$(name3)</display_name>
      <visible>true</visible>
      <name>$(P3)$(R3)AvgValue</name>
      <axis>0</axis>
      <color>
        <red>0</red>
        <green>128</green>
        <blue>0</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
      <archive>
        <name>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</name>
        <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
        <key>1</key>
      </archive>
    </pv>
    <pv>
      <display_name>$(name4)</display_name>
      <visible>true</visible>
      <name>$(P4)$(R4)AvgValue</name>
      <axis>0</axis>
      <color>
        <red>127</red>
        <green>0</green>
        <blue>255</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
      <archive>
        <name>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</name>
        <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
        <key>1</key>
      </archive>
    </pv>
    <pv>
      <display_name>$(name5)</display_name>
      <visible>true</visible>
      <name>$(P5)$(R5)AvgValue</name>
      <axis>0</axis>
      <color>
        <red>128</red>
        <green>0</green>
        <blue>0</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
      <archive>
        <name>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</name>
        <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
        <key>1</key>
      </archive>
    </pv>
  </pvlist>
</databrowser>
